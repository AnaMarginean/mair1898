package biblioteca.util;

import biblioteca.model.Carte;

public class Validator {

	public static boolean isAuthorValid(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z]+");
		if(flag == false)
			throw new Exception("Autor invalid!");
		return flag;
	}

	public static void validateCarte(Carte c)throws Exception{
		if(c.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(c.getAutori()==null){
			throw new Exception("Lista autori vida!");
		}
		if(!isStringValid(c.getTitlu()))
			throw new Exception("Titlu invalid!");
		for(String s:c.getAutori()){
			if(!isStringValid((s)))
				throw new Exception("Autor invalid!");
		}
		for(String s:c.getCuvinteCheie()){
			if(!isStringValid(s))
				throw new Exception("Cuvant cheie invalid!");
		}

		isYearValid(c.getAnAparitie().toString());
	}

	public static void isYearValid(String year) throws Exception{
		if(Integer.parseInt(year) < 1800)
			throw new Exception("An aparitie < 1800!");

		if(Integer.parseInt(year) > 2018)
			throw new Exception("An aparitie > 2018!");
	}

	public static boolean isStringValid(String s){
		String []t = s.split(" ");
		if(t.length==2){
			boolean ok1 = t[0].matches("[a-zA-Z]+");
			boolean ok2 = t[1].matches("[a-zA-Z]+");
			if(ok1==ok2 && ok1==true){
				return true;
			}
			return false;
		}
		return s.matches("[a-zA-Z]+");
	}
	
}
