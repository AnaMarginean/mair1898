package biblioteca.controller;


import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepositoryInterface;
import biblioteca.util.Validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiController {

	private CartiRepositoryInterface cartiRepository;
	
	public CartiController(CartiRepositoryInterface cartiRepository){
		this.cartiRepository = cartiRepository;
	}
	
	public void adaugaCarte(Carte c) throws Exception{
		Validator.validateCarte(c);
		cartiRepository.adaugaCarte(c);
	}
	
	public void modificaCarte(Carte nou, Carte vechi) throws Exception{
		cartiRepository.modificaCarte(nou, vechi);
	}
	
	public void stergeCarte(Carte c) throws Exception{
		cartiRepository.stergeCarte(c);
	}

	public List<Carte> cautaCarteDupaAutor(String autor) throws Exception {
		/*1*/Validator.isAuthorValid(autor);

		List<Carte> carti = getCarti();
		List<Carte> cartiGasite = new ArrayList<Carte>();
		int i=0;
		/*2*/while (i<carti.size()){
			/*3*/boolean flag = false;
			List<String> lref = carti.get(i).getAutori();
			int j = 0;
			/*4*/while(j<lref.size()){
				/*5*/if(lref.get(j).toLowerCase().contains(autor.toLowerCase())){
					/*6*/flag = true;
					break;
				}
				/*7*/j++;
			}
			/*8*/if(flag == true){
				/*9*/cartiGasite.add(carti.get(i));
			}
			/*10*/i++;
		}
		/*11*/return cartiGasite;
	}
	
	public List<Carte> getCarti() throws Exception{
		return cartiRepository.getCarti();
	}
	
	public List<Carte> getCartiOrdonateDinAnul(String an) throws Exception{
		Validator.isYearValid(an);

		List<Carte> lc = getCarti();
		List<Carte> lca = new ArrayList<Carte>();

		for(Carte c:lc){
			if(c.getAnAparitie().toString().equals(an)){
				lca.add(c);
			}
		}

		Collections.sort(lca,new Comparator<Carte>(){

			@Override
			public int compare(Carte a, Carte b) {
				if(a.getTitlu().compareTo(b.getTitlu())==0){
					return a.getAutori().get(0).compareTo(b.getAutori().get(0));
				}

				return a.getTitlu().compareTo(b.getTitlu());
			}

		});

		return lca;
	}
}
