package biblioteca;

import biblioteca.controller.CartiController;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BigBang {
    private CartiRepository cartiRepository;
    private CartiController cartiController;

    @Before
    public void init(){
        cartiRepository = new CartiRepository("cartiTest.txt");
        cartiController = new CartiController(cartiRepository);
        clearFile();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void TC_A() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),1990, new ArrayList<String>(Arrays.asList("Cuvant", "Cuvant")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
    }

    @Test
    public void TC_B() throws Exception {
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("a")),1990, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
        cartiController.adaugaCarte(carte);
        List<Carte> carti = cartiController.cautaCarteDupaAutor("a");
        assertEquals(carti.size(), 1);
    }

    @Test
    public void TC_C() throws Exception {
        Carte carte1 = new Carte("Titlu b", "Editura", new ArrayList<String>(Arrays.asList("a")),1990, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
        Carte carte2 = new Carte("Titlu a", "Editura", new ArrayList<String>(Arrays.asList("a")),1990, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
        Carte carte3 = new Carte("Titlu c", "Editura", new ArrayList<String>(Arrays.asList("a")),2000, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
        cartiController.adaugaCarte(carte1);
        cartiController.adaugaCarte(carte2);
        cartiController.adaugaCarte(carte3);
        List<Carte> carti = cartiController.getCartiOrdonateDinAnul("1990");
        assertEquals(carti.size(), 2);
        assertEquals(carti.get(0).getTitlu(), "Titlu a");
    }

    @Test
    public void TC_P() throws Exception {
        clearFile();

        Integer sizeBefore = cartiController.getCarti().size();
        Carte cartex = new Carte("Titlu x", "Editura x", new ArrayList<String>(Arrays.asList("Autor x")),1990, new ArrayList<String>(Arrays.asList("Cuvant x")));
        Carte cartey = new Carte("Titlu y", "Editura y", new ArrayList<String>(Arrays.asList("Autor y")),2000, new ArrayList<String>(Arrays.asList("Cuvant y")));
        Carte cartez = new Carte("Titlu z", "Editura z", new ArrayList<String>(Arrays.asList("Autor z")),2010, new ArrayList<String>(Arrays.asList("Cuvant z")));
        cartiController.adaugaCarte(cartex);
        cartiController.adaugaCarte(cartey);
        cartiController.adaugaCarte(cartez);
        assertEquals(sizeBefore + 3, cartiController.getCarti().size());

        List<Carte> cartiDupaAutor = cartiController.cautaCarteDupaAutor("x");
        assertEquals(cartiDupaAutor.size(), 1);

        List<Carte> cartiDupaAn = cartiController.getCartiOrdonateDinAnul("2000");
        assertEquals(cartiDupaAn.size(), 1);
        assertEquals(cartiDupaAn.get(0).getTitlu(), "Titlu y");

        clearFile();
    }

    @After
    public void finish(){
        clearFile();
    }

    public void clearFile() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("cartiTest.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        writer.print("");
        writer.close();
    }
}
