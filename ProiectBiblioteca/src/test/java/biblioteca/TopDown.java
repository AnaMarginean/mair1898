package biblioteca;

import biblioteca.controller.CartiController;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TopDown {
    private CartiRepository cartiRepository;
    private CartiController cartiController;

    @Before
    public void init(){
        cartiRepository = new CartiRepository("cartiTest.txt");
        cartiController = new CartiController(cartiRepository);
        clearFile();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void TC_A() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu x", "Editura x", new ArrayList<String>(Arrays.asList("Autor x")),1990, new ArrayList<String>(Arrays.asList("Cuvant x")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
    }

    @Test
    public void TC_AB() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu y", "Editura y", new ArrayList<String>(Arrays.asList("Autor y")),2000, new ArrayList<String>(Arrays.asList("Cuvant y")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
        List<Carte> carti = cartiController.cautaCarteDupaAutor("y");
        assertEquals(carti.size(), 1);
    }

    @Test
    public void TC_ABC() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu z", "Editura z", new ArrayList<String>(Arrays.asList("Autor z")),2010, new ArrayList<String>(Arrays.asList("Cuvant z")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
        List<Carte> cartiDupaAutor = cartiController.cautaCarteDupaAutor("z");
        assertEquals(cartiDupaAutor.size(), 1);
        List<Carte> cartiDupaAn = cartiController.getCartiOrdonateDinAnul("2010");
        assertEquals(cartiDupaAn.size(), 1);
        assertEquals(cartiDupaAn.get(0).getTitlu(), "Titlu z");
    }

    @After
    public void finish(){
        clearFile();
    }

    public void clearFile() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("cartiTest.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        writer.print("");
        writer.close();
    }
}
