package biblioteca;

import biblioteca.controller.CartiController;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Requirement3 {
        private CartiRepository cartiRepository;
        private CartiController cartiController;

        @Before
        public void init(){
            cartiRepository = new CartiRepository("cartiTest.txt");
            cartiController = new CartiController(cartiRepository);
            clearFile();
        }

        @Rule
        public ExpectedException expectedEx = ExpectedException.none();

        @Test
        public void TC01() throws Exception {
            Carte carte1 = new Carte("Titlu b", "Editura", new ArrayList<String>(Arrays.asList("a")),1990, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
            Carte carte2 = new Carte("Titlu a", "Editura", new ArrayList<String>(Arrays.asList("a")),1990, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
            Carte carte3 = new Carte("Titlu c", "Editura", new ArrayList<String>(Arrays.asList("a")),2000, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
            cartiController.adaugaCarte(carte1);
            cartiController.adaugaCarte(carte2);
            cartiController.adaugaCarte(carte3);
            List<Carte> carti = cartiController.getCartiOrdonateDinAnul("1990");
            assertEquals(carti.size(), 2);
            assertEquals(carti.get(0).getTitlu(), "Titlu a");
        }

        @Test
        public void TC02() throws Exception {
            expectedEx.expect(Exception.class);
            expectedEx.expectMessage("An aparitie < 1800!");
            cartiController.getCartiOrdonateDinAnul("1600");
        }

    @After
    public void finish(){
        clearFile();
    }

        public void clearFile() {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter("cartiTest.txt");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            writer.print("");
            writer.close();
        }
}
