package biblioteca;

import biblioteca.controller.CartiController;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class Requirement1 {
    private CartiRepository cartiRepository;
    private CartiController cartiController;

    @Before
    public void init(){
        cartiRepository = new CartiRepository("cartiTest.txt");
        cartiController = new CartiController(cartiRepository);
        clearFile();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void TC1_ECP() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),1990, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
    }

    @Test
    public void TC5_ECP() throws Exception {
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("An aparitie < 1800!");
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),1700, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
    }

    @Test
    public void TC6_ECP() throws Exception {
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("An aparitie > 2018!");
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),30000, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
    }

    @Test
    public void TC1_BVA() throws Exception {
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("Titlu invalid!");
        Carte carte = new Carte("", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),1990, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
    }

    @Test
    public void TC3_BVA() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),1990, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
    }

    @Test
    public void TC9_BVA() throws Exception {
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("An aparitie < 1800!");
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),1799, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
    }

    @Test
    public void TC10_BVA() throws Exception {
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("An aparitie > 2018!");
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),2019, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
    }

    @Test
    public void TC11_BVA() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),1800, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
    }

    @Test
    public void TC12_BVA() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),2018, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
    }

    @Test
    public void TC13_BVA() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),2017, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
    }

    @Test
    public void TC14_BVA() throws Exception {
        Integer sizeBefore = cartiController.getCarti().size();
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("Autor", "Autor")),2000, new ArrayList<String>(Arrays.asList("Autor", "Autor")));
        cartiController.adaugaCarte(carte);
        assertEquals(sizeBefore + 1, cartiController.getCarti().size());
    }

    @After
    public void finish(){
        clearFile();
    }

    public void clearFile() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("cartiTest.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        writer.print("");
        writer.close();
    }
}
