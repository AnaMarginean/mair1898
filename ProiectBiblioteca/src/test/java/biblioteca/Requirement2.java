package biblioteca;

import biblioteca.controller.CartiController;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Requirement2 {
    private CartiRepository cartiRepository;
    private CartiController cartiController;

    @Before
    public void init(){
        cartiRepository = new CartiRepository("cartiTest.txt");
        cartiController = new CartiController(cartiRepository);
        clearFile();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void TC01() throws Exception {
        List<Carte> carti = cartiController.cautaCarteDupaAutor("a");
        assertEquals(carti.size(), 0);
    }

    @Test
    public void TC02() throws Exception {
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("a", "b")),1990, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
        cartiController.adaugaCarte(carte);
        List<Carte> carti = cartiController.cautaCarteDupaAutor("x");
        assertEquals(carti.size(), 0);
    }

    @Test
    public void TC03() throws Exception {
        Carte carte1 = new Carte("Titlu", "Editura", new ArrayList<String>(),1990, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
        Carte carte2 = new Carte("Titlu", "Editura", new ArrayList<String>(),1990, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
        cartiController.adaugaCarte(carte1);
        cartiController.adaugaCarte(carte2);
        List<Carte> carti = cartiController.cautaCarteDupaAutor("a");
        assertEquals(carti.size(), 0);
    }

    @Test
    public void TC04() throws Exception {
        Carte carte = new Carte("Titlu", "Editura", new ArrayList<String>(Arrays.asList("a")),1990, new ArrayList<String>(Arrays.asList("Cuvant cheie")));
        cartiController.adaugaCarte(carte);
        List<Carte> carti = cartiController.cautaCarteDupaAutor("a");
        assertEquals(carti.size(), 1);
    }

    @Test
    public void TC05() throws Exception {
        expectedEx.expect(Exception.class);
        expectedEx.expectMessage("Autor invalid!");
        List<Carte> carti = cartiController.cautaCarteDupaAutor("2");
    }

    @After
    public void finish(){
        clearFile();
    }

    public void clearFile() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("cartiTest.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        writer.print("");
        writer.close();
    }
}
